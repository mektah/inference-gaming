'''
Created on 24.05.2012

@author: moritz
'''
import pygame, pygame.font, pygame.time, sys
import config, helper

class WeaponScreen(pygame.Surface):
    def __init__(self, objectlist, keylist, background, logger):
        pygame.Surface.__init__(self, (config.width, config.height))
        self.numberOfWeapons = len(objectlist)
        self.blit(background, (0, 0))
        '''
        only show weapon screen if there is more than one weapon to choose from
        ''' 
        if self.numberOfWeapons > 1:
            font = pygame.font.Font(None, 72)
            text = font.render("Choose your weapon:", 1, config.textcolor)
            textpos = text.get_rect(center = (config.width/2, config.height/4))
            self.blit(text, textpos)
        self.log = logger
        self.objectlist = objectlist
        self.keylist = keylist
        self.poslist = []
            
        imagesize = config.playersize[0] + 10
        start =  config.width/2 - (self.numberOfWeapons / 2) * imagesize
        
        if self.numberOfWeapons > 1:
            for k in range(self.numberOfWeapons):
                imagepos = [k*imagesize + start ,config.height/4 + 100]
                objectlist[k].moveto(imagepos)
                self.blit(objectlist[k].image, objectlist[k].pos)
                font = pygame.font.Font(None, 36)
                text = font.render(str(pygame.key.name(keylist[k])),  1, config.textcolor)
                self.blit(text, (imagepos[0], imagepos[1] + 10 + config.playersize[1]))
                self.poslist.append(objectlist[k].rect)
    
    '''
    '''
    def chooseWeapon(self):
        if self.numberOfWeapons == 1: # if there is only one weapon, return it
            return self.objectlist[0]
        clock = pygame.time.Clock()
        while 1: # waiting for mouse click on weapon image of key press
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.onClose()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE or event.key == pygame.K_F7:
                        self.onClose()
                    for k in range(self.numberOfWeapons):
                        if event.key == self.keylist[k]:
                            return self.objectlist[k]
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    for k in range(self.numberOfWeapons):
                        if self.poslist[k].collidepoint(event.pos):
                            return self.objectlist[k]
                        
            clock.tick(20)
            
    def onClose(self):
        self.log.plotpos()
        self.log.closeLogFiles() 
        sys.exit()