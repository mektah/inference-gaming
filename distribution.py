'''
Created on 22.05.2012

This class is in charge of all probabilistic elements in the game.
It generates a gaussian height distribution for enemy spawns, side of enemy spawns and the probalistic rewards
@author: Moritz v. Looz
'''

import random, config

class Distribution:
    def __init__(self, log):        
        self.threshold = config.initThreshold
        self.counter = 0
        self.log = log
                
    def getSpawnHeight(self):
        if config.gaussianheight:
            height = random.gauss(config.height/4, config.height/8)
        else:
            height = config.height/4
        if height < 0:
            height = 0
        if height > config.height - 100:
            height = config.height - 100
        return int(height)
        
    def getSpawnSide(self):
        self.counter += 1
        if self.counter % config.trialsBetweenProbabilityShift == 0:
            self.counter = 0
            self.threshold = random.random()
            #self.log.writeline("Changed probability of enemy appearing left to" + str(self.threshold))
            
        if random.random() > self.threshold:
            return 1
        else:
            return 0
        
    def getReward(self, enemy, weapon, elapsed_time):
        baseReward = enemy.reward * weapon.multiplier
        if config.timedependantReward:
            mu = float(baseReward) / (float(elapsed_time)/1000)
        else:
            mu = baseReward
        sigma = mu/2
        if config.gaussianReward:            
            return max(int(random.gauss(mu, sigma)), config.minReward)
        else:
            return max(int(mu), config.minReward)
