'''
Created on 22.05.2012

Several methods for use in other classes
Handels file loading/saving, standard levels and math
@author: Moritz v. Looz
'''

import pygame, config, os, math, pickle
from gobjects import Wave, Saucer, Tripod

def load_image(name):
    path = os.path.join(os.path.dirname(__file__), config.datapath)
    try:
        image = pygame.image.load(os.path.join(path, name)).convert_alpha()
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)
    return image

def loadWaves(levelfile):
    filehandle = open(levelfile, 'r')
    levels = pickle.load(filehandle)
    filehandle.close()
    return levels
        
def saveWaves(levels, levelfile):
    filehandle = open(levelfile, 'w')
    pickle.dump(levels, filehandle)

def createLogFile(name, directory = None):
    path = name
    if not directory == None:
        path = os.path.join(directory, path) 
    f = open(path, 'w')
    return f

def logprefix(name):
    return os.path.join(config.logpath, name)

def ensureDir(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)
        
def calculateBoltspeed(degrees):
    boltspeed = [math.sin(math.radians(degrees))*config.boltspeed, math.cos(math.radians(degrees))*config.boltspeed]
    return boltspeed
        
def calculateAngle(diffarray):
    if diffarray[0] == 0:
        if diffarray[1] > 0:
            return 0
        else:
            return 180
    # now diffarray 0 is nonzero
    if diffarray[1] == 0:
        if diffarray[0] > 0:
            return 90
        else:
            return 270
        
    # both are nonzero        
    return math.degrees(math.atan(float(diffarray[0]) / float(diffarray[1])))

'''
levels used if game is called with no arguments
'''
def generateStandardLevels(dist):
    levels = []
    for i in range(config.trialstotal):
        right = dist.getSpawnSide()
        spawnheight = dist.getSpawnHeight()
        waves = []
        if i % 2:
            twave = Wave(spawnheight, 1-right, Saucer, 5, config.saucerreward, config.saucerfireprobability, config.saucerspeed*3)
            waves.append(twave)
        else:
            twave = Wave(spawnheight, right, Tripod, 10, config.tripodreward, config.tripodfireprobability, config.tripodspeed)
            swave = Wave(spawnheight, 1-right, Saucer, 1, config.saucerreward, config.saucerfireprobability, config.saucerspeed)
            twave2 = Wave(spawnheight + 50, right, Tripod, 10, config.tripodreward, 0, config.tripodspeed)
            waves.append(twave)
            waves.append(swave)
            waves.append(twave2)
        levels.append(waves)
    return levels

'''
levels used if game is called with --simple, but no levelfile
'''
def generateSimpleLevels(dist):
    levels = []
    for i in range(config.trialstotal):
        right = dist.getSpawnSide()
        waves = []
        spawnheight = config.height/4
        twave = Wave(spawnheight, right, Tripod, 1, config.tripodreward, 0, config.tripodspeed)
        waves.append(twave)
        levels.append(waves)
    return levels
