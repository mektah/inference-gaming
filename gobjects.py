'''
Created on 22.05.2012

@author: moritz
'''
import pygame, pygame.font
import config, helper
import random

class GameObject(pygame.sprite.Sprite):
    def __init__(self, image, pos, speed, width = config.width, height = config.height):
        pygame.sprite.Sprite.__init__(self)
        self.image = image        
        self.pos = image.get_rect().move(pos)
        self.speed = speed
        self.width = width
        self.height = height
        self.rect = self.pos
        
    def move(self, speed):        
        self.pos = self.pos.move(speed)
        self.rect = self.pos
        
    def moveto(self, destination):
        self.pos = self.image.get_rect().move(destination)
        self.rect = self.pos
        
    def update(self):
        self.move(self.speed)
    
class EnemyObject(GameObject):
    def __init__(self, image, pos, speed, screentime):
        GameObject.__init__(self, image, pos, speed)
        self.reverse = 0
        self.reversecount = 0
        self.screentime = screentime
    
    def update(self):
        self.move(self.speed)        
        if self.pos.left < 0 or self.pos.right > self.width:
            self.reverse = 1
        else:
            self.reverse = 0
        
        if self.pos.right < 0 or self.pos.left > self.width:
            self.kill()
            
    def hasToReverse(self):
        return self.reverse
    
    def doReverse(self):
        if self.reversecount < self.screentime:
            self.move((0,20))
            self.speed[0] = -self.speed[0] 
        self.reversecount += 1
    
    def fire(self, boltimage, boltlist, target):
        boltpos = [(self.pos.left + self.pos.right) /2, self.pos.bottom]
        if self.aims:
            boltspeed = (target.pos[0]-boltpos[0],target.pos[1]-boltpos[1])
        else:
            boltspeed = (0,1)
        sumspeed = abs(boltspeed[0]) + abs(boltspeed[1])
        boltimage = pygame.transform.rotate(boltimage, helper.calculateAngle(boltspeed))
        cfactor = float(config.boltspeed) / float(sumspeed)        
        bolt = Bolt(boltimage, boltpos, (boltspeed[0] * cfactor,boltspeed[1]*cfactor))
        boltlist.add(bolt)
        return bolt
    
    def mayFire(self, boltimage, boltlist, target):
        if random.random() < self.fireprobability:
            return self.fire(boltimage, boltlist, target)
        else:
            return []
    
class Tripod(EnemyObject):
    defaultimage = 0
    size = config.tripodsize
    
    def __init__(self, image, pos, speed, fireprobability = config.tripodfireprobability, reward = config.tripodreward):
        EnemyObject.__init__(self, image, pos, speed, config.tripodscreentime)
        self.aims = config.tripodaim
        self.fireprobability = fireprobability
        self.reward = reward
    
class Saucer(EnemyObject):
    defaultimage = 0
    size = config.saucersize
    
    def __init__(self, image, pos, speed, fireprobability = config.saucerfireprobability, reward = config.saucerreward):
        EnemyObject.__init__(self, image, pos, speed, config.saucerscreentime)
        self.aims = config.sauceraim
        self.fireprobability = fireprobability
        self.reward = reward
    
class Bolt(GameObject):
    def update(self):
        self.move(self.speed)
        if self.pos.bottom < 0 or self.pos.top > config.height:
            self.kill()
        if self.pos.right < 0 or self.pos.left > config.width:
            self.kill()
            
class Number(GameObject):
    def __init__(self, number, pos, fontsize=36, color=config.textcolor):
        font = pygame.font.Font(None, fontsize)
        text = font.render(str(number), 1, color)
        GameObject.__init__(self, text, pos, (0, -config.textspeed))
        
    def update(self):
        self.move(self.speed)
        if self.pos.bottom < 0:
            self.kill()
        if self.pos.right < 0 or self.pos.left > config.width:
            self.kill()
            
class Weapon(GameObject):
    def __init__(self, image , pos):
        GameObject.__init__(self, image, pos, (0,0))
    
    def fire(self, boltimage, boltlist, target = None):
        bolt = Bolt(boltimage, ((self.pos.left + self.pos.right) /2, self.pos.top), (0,-config.boltspeed))
        boltlist.add(bolt)
        return bolt
        
    def moveControlled(self, speed):
        if self.pos.left + speed[0] >= 0 and self.pos.right + speed[0] <= self.width:
            self.move(speed)
        
class BasicGun(Weapon):
    cooldown = config.gunfirecooldown
    multiplier = config.gunrewardmultiplier
    
    def fire(self, boltimage, boltlist):
        bolt = Bolt(boltimage, ((self.pos.left + self.pos.right) /2, self.pos.top), (0,-config.boltspeed))
        boltlist.add(bolt)
        return bolt
        
class LaserShotGun(Weapon):
    cooldown = config.shotgunfirecooldown
    multiplier = config.shotgunrewardmultiplier
    
    def fire(self, boltimage, boltlist):
        degreestep = float(config.shotgunspread) / float((config.shotgunprojectiles -1))
        startangle = config.shotgunspread / 2
        bolts = []        
        for i in range(config.shotgunprojectiles):            
            currentangle = startangle - i*degreestep
            boltspeed = helper.calculateBoltspeed(currentangle + 180)
            bolt = Bolt(pygame.transform.rotate(boltimage, currentangle), ((self.pos.left + self.pos.right) /2, self.pos.top), boltspeed)
            boltlist.add(bolt)
            bolts.append(bolt)
        return bolts
    
class Wave():
    def __init__(self, height, side, enemytype, number, reward, fireprobability, speed):
        self.height = height
        self.side = side
        self.enemytype = enemytype
        self.number = number
        self.reward = reward
        self.fireprobability = fireprobability
        self.speed = speed