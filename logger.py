'''
Created on 22.05.2012

Creates log files for each trial and plots enemy and player positions with gnuplot
@author: Moritz v. Looz
'''

import helper, time, config, os
try:
    import Gnuplot, Gnuplot.funcutils
    gnuplotinstalled = 1
except ImportError:
    gnuplotinstalled = 0

class Logger:
    def __init__(self):
        name = "triallog-" + str(time.time())                
        self.logdir = helper.logprefix(name)
        helper.ensureDir(self.logdir)
        self.filehandle = helper.createLogFile("story.txt", self.logdir)
        self.playerposcsv = helper.createLogFile("playerpos.csv", self.logdir)
        self.playermovcsv = helper.createLogFile("playermov.csv", self.logdir)
        self.enemyspawncsv = helper.createLogFile("enemyspawn.csv", self.logdir)
        self.shotsfiredcsv = helper.createLogFile("shotsfired.csv", self.logdir)
        self.beginposcsv = helper.createLogFile("beginpos.csv", self.logdir)
        self.enemyspawnlist = []
        self.beginposlist = []        
        
    def write(self, string):
        self.filehandle.write(string)
        
    def writeline(self, string):
        self.filehandle.write(string + "\n")
        
    def dumpConfig(self):        
        self.separator()
        self.write("State of config file:")
        self.writeline("")
        f = open('config.py', 'r')
        for line in f:
            self.write(line)
        self.writeline("")
        self.separator()        
        
    def separator(self):
        self.writeline("------------------------------------")
        
    def playerpos(self, trial, ticks, position):
        self.playerposcsv.write(str(trial) + ", " + str(ticks) + ", " + str(position) + "\n")
        
    def playermov(self, trial, ticks, position, keyevent):
        self.playermovcsv.write(str(trial) + ", " + str(ticks) + ", " + str(position) + ", " + keyevent + "\n")
        
    def enemyspawn(self, trial, ticks, position):
        self.enemyspawncsv.write(str(trial) + ", " + str(ticks) + ", " + str(position) + "\n")
        self.enemyspawnlist.append(position[0])
        
    def shotsfired(self, trial, ticks, position):
        self.shotsfiredcsv.write(str(trial) + ", " + str(ticks) + ", " + str(position) + "\n")
        
    def beginpos(self, trial, position):
        self.beginposcsv.write(str(trial) + ", " + str(position) + "\n")
        self.beginposlist.append(position)
        
    def plotpos(self):
        if gnuplotinstalled:
            g = Gnuplot.Gnuplot(debug=1)        
            g('set style data linespoints')
            g('set terminal png')
            g('set yrange [' + str(-10) + ':' + str(config.width + 100) + ']')
            player = Gnuplot.Data(self.beginposlist, title='Player Positions')
            enemies = Gnuplot.Data(self.enemyspawnlist, title='Enemy Positions', with_='points')        
            g.title("Enemy and player positions")
            plotfile = os.path.join(self.logdir, "positions.png")      
            g('set output "' + plotfile + '"')
            g.plot(enemies, player)        
        
    def closeLogFiles(self):
        self.filehandle.close()
        self.playerposcsv.close()
        self.playermovcsv.close()
        self.enemyspawncsv.close()
        self.shotsfiredcsv.close()
        self.beginposcsv.close()