'''
Main game logic and game loop.
Build with Python 2.7.3, pygame and python-gnuplot
@author: Moritz v. Looz
'''

import sys, getopt #python imports
import pygame.font, pygame.time #pygame imports
import distribution, helper, logger, screens, inputdevice, config #game modules
from gobjects import BasicGun, LaserShotGun, Number, Saucer, Tripod #game objects
from config import size, width, height, backgroundfile, tripodfile, tripodsize, saucerfile, saucersize, boltfile, boltsize, playersize, gunfile, shotgunfile, penalty #constants

pygame.init()

# setting up the background
#------------------------------------------------------------------------------ 

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Inferential Gaming")

obg = helper.load_image(backgroundfile)
background = obg.copy()

if pygame.font:
    font = pygame.font.Font(None, 36)
    text = font.render("Points:", 1, (200, 200, 100))
    textpos = text.get_rect(center = (50, 50))
    background.blit(text, textpos)
    text = font.render("Round:", 1, (200, 200, 100))
    textpos = text.get_rect(center = (width - 100, 50))
    background.blit(text, textpos)
    
screen.blit(background, (0,0))
pygame.display.flip()

clock = pygame.time.Clock()
log = logger.Logger()

#loading enemy images
#------------------------------------------------------------------------------ 

tripodimage = helper.load_image(tripodfile)
tripodimage = pygame.transform.scale(tripodimage, tripodsize)
Tripod.defaultimage = tripodimage

saucerimage = helper.load_image(saucerfile)
saucerimage = pygame.transform.scale(saucerimage, saucersize)
Saucer.defaultimage = saucerimage

enemies = pygame.sprite.Group()

dist = distribution.Distribution(log)

#loading player images
#------------------------------------------------------------------------------ 

boltimage = helper.load_image(boltfile)
boltimage = pygame.transform.scale(boltimage, boltsize)
playerbolts = pygame.sprite.Group()
enemybolts = pygame.sprite.Group()
playergroup = pygame.sprite.GroupSingle()

playerspos = (width/2, height -playersize[0] - 10)

gameobjects = pygame.sprite.Group()
gameobjects.add(playerbolts, enemybolts)

gunimage = pygame.transform.scale(helper.load_image(gunfile), playersize)
shotgunimage = pygame.transform.scale(helper.load_image(shotgunfile), playersize)
gun = BasicGun(gunimage, playerspos)
shotgun = LaserShotGun(shotgunimage, playerspos)
weaponscreen = screens.WeaponScreen([gun, shotgun], [pygame.K_g, pygame.K_f], obg, log)
singleweaponscreen = screens.WeaponScreen([gun], [pygame.K_g], obg, log)


inputdev = inputdevice.Keyboard()
# uncomment this to enable joystick support. Still a bit buggy.
#inputdev = inputdevice.Joystick()

#parser = argparse.ArgumentParser(description='Start an instance of inference space invaders')
#parser.add_argument()

optlist, leftoverargs = getopt.getopt(sys.argv[1:], '', ['levels=','config=','simple'])

levels = []

for option, argument in optlist:
    if option == '--levels':
        levels = helper.loadWaves(argument)
        if not levels:
            print("Failed to load levelfile, using standard levels")
    if option == '--config':
        print("Config option not yet implemented")
    if option == '--simple':
        weaponscreen = singleweaponscreen
        if not levels:
            levels = helper.generateSimpleLevels(dist)
            helper.saveWaves(levels, "simple")
        
print(leftoverargs)

if not levels:
    levels = helper.generateStandardLevels(dist)
    helper.saveWaves(levels, "levels")

leftdown = 0
rightdown = 0
trialcounter = 0
totalpoints = 0
lastfired = 0

log.write("New game started.\n")
#log.dumpConfig()

'''
New level starts here
A wave is a column of enemies, consisting of a single type and moving in the same direction
A level is a set of waves, stacked vertically. The player can choose a new weapon once per level.
If you want to generate enemies dynamically in response to player skill, change this 'for' loop and add new waves at the end.
'''
for waves in levels:
    screen.blit(weaponscreen, (0,0))
    pygame.display.update()
    pygame.mouse.set_visible(1)
    player = weaponscreen.chooseWeapon()
    pygame.mouse.set_visible(0)
    player.moveto(playerspos)
    playergroup.add(player)
    gameobjects.add(playergroup)
    leftdown = 0 # pygame.key.get_pressed()[102]
    rightdown = 0# pygame.key.get_pressed()[103]
    
    trialcounter += 1
    screen.blit(background, (0, 0))
    pygame.display.flip()    
    endtime = 0
    points = 0
    enemiesadded = 0
    gamerunning = 1
    enemycolumns = []
    
    inputdev.reset()
    
    log.writeline("Starting Trial " + str(trialcounter) + ".")
    
    starttime = pygame.time.get_ticks()
    currenttime = starttime
    
    '''
    inner game loop, ends a while after player or enemies are dead/vanished
    enemies and projectiles move
    '''
    while gamerunning or currenttime - endtime < config.endtimedelay:
        currenttime = pygame.time.get_ticks()
        
        '''
        the following block is only called once per level.
        It is in the inner game loop to allow a small time delay for the player to move
        to the position where he/she expects enemies
        '''
        if not enemiesadded and currenttime - starttime > config.enemydelay:
            log.writeline("Player is at position " + str(player.pos.centerx))
            log.beginpos(trialcounter, player.pos[0])
            # add waves
            for nextwave in waves:
                dirstring = "left"
                if nextwave.side:
                    dirstring = "right"
                log.writeline("Enemies appear from " + dirstring)
                column = pygame.sprite.Group()
                size = nextwave.enemytype.size
                enemylogpos = (nextwave.side * width, nextwave.height)
                for x in range(nextwave.number):
                    spawnspeed = [nextwave.speed, 0]
                    if nextwave.side:
                        pos = (width - 50 - x*size[0], nextwave.height)
                        spawnspeed = [-nextwave.speed,0]
                    else:
                        pos = (x*size[0], nextwave.height)
                        spawnspeed = [nextwave.speed, 0]
                    o = nextwave.enemytype(nextwave.enemytype.defaultimage, pos, spawnspeed, nextwave.fireprobability)
                    column.add(o)
                    
                enemycolumns.append(column)
                enemies.add(column)            
            log.enemyspawn(trialcounter, starttime, enemylogpos)
            enemiesadded = 1 # this block won't be called again until the next level 
            gameobjects.add(enemies)
            
        # remove game objects to paint them on their new positions
        gameobjects.clear(screen, background)
        
        inputdev.gatherevents()
        inputdev.getplayerspeed()
        
        if inputdev.hasquit():
            log.plotpos()
            log.closeLogFiles() 
            sys.exit()
            
        if config.mouseenabled and inputdev.mousemoved:
            player.moveto((pygame.mouse.get_pos()[0], player.pos[1]))
            pygame.mouse.set_pos(player.pos[0], player.pos[1])
            
        '''
        player can only shoot if still alive
        maximum rate of fire is determined by weapon cooldown 
        '''
        if inputdev.hasfired() and player.alive():
            if currenttime - lastfired >= player.cooldown:
                player.fire(boltimage, playerbolts)
                lastfired = currenttime
                log.shotsfired(trialcounter, currenttime, player.pos[0])
                gameobjects.add(playerbolts)
    
        # if mouse and keyboard are used at the same time, strange things happen
        player.moveControlled(inputdev.getplayerspeed())
                
        reverse = 0
        
        # move all enemies and bolts to their new positions
        gameobjects.update()
        
        for c in enemycolumns:
            reverse = 0
            for e in c:
                # fire at the player, or not
                bolt = e.mayFire(boltimage, enemybolts, player)
                # if enemy column touches border, reverse direction
                if e.hasToReverse():
                    reverse = 1
                # if enemy touches bottom, player looses
                if e.pos.bottom > height:
                    endtime = currenttime
                    gamerunning = 0
            if reverse:
                for e in c:
                    e.doReverse()
                
        gameobjects.add(enemybolts)
        
        # kill player if hit by enemy weapon fire
        deadplayer = pygame.sprite.groupcollide(playergroup, enemybolts, 1,1)
        if deadplayer:
            endtime = currenttime
            gamerunning = 0
            points -= penalty
            totalpoints -= penalty
            player.kill()
            if penalty > 0:
                gameobjects.add(Number(penalty, (player.pos.centerx, player.pos.centery), color=config.warncolor))
        
        # kill enemies hit by player weapon fire
        killedships =  pygame.sprite.groupcollide(enemies, playerbolts, 1, 1)
        killedtime = pygame.time.get_ticks()
        for o in killedships:
            screen.blit(background, o.pos, o.pos)
            reward = dist.getReward(o, player, killedtime - starttime - config.enemydelay)
            points += reward
            totalpoints += reward
            gameobjects.add(Number(reward, (o.pos.centerx, o.pos.centery)))
            
        # if no enemies are left, end level
        if enemies.__len__() == 0 and enemiesadded and gamerunning:
            endtime = currenttime
            gamerunning = 0
            
        gameobjects.draw(screen)
        
        # plot points and round
        font = pygame.font.Font(None, 36)
        text = font.render(str(totalpoints), 1, config.textcolor)
        textpos = text.get_rect(center = (150, 50))
        screen.blit(background, textpos.inflate(40,0), textpos.inflate(40,0))
        screen.blit(text, textpos)
        
        font = pygame.font.Font(None, 36)
        text = font.render(str(trialcounter), 1, config.textcolor)
        textpos = text.get_rect(center = (width - 30, 50))
        screen.blit(background, textpos, textpos)
        screen.blit(text, textpos)
        
        pygame.display.update()
        log.playerpos(trialcounter, pygame.time.get_ticks(), player.pos[0])
        clock.tick(50)
    # end of level
    screen.blit(background, (0,0)) # clear screen
    playerbolts.empty() #remove bolts and enemies. (enemies.empty() MAKES the group empty, doesn't test for emptiness)
    enemybolts.empty()
    gameobjects.empty()
    
    #player has won if all enemies are dead
    if enemies.__len__() == 0:
        font = pygame.font.Font(None, 72)
        text = font.render("You won " + str(points) + " points!", 1, (200, 200, 100))            
        textpos = (width/4, height/4)
        screen.blit(text, textpos)            
        endtime = pygame.time.get_ticks()
        log.write("Player wins trial after " + str((endtime - starttime)/1000) + " seconds,")
        log.writeline(" scoring " + str(points) + " points")
    else:
        enemies.empty()        
        font = pygame.font.Font(None, 72)
        text = font.render("You lost!", 1, config.textcolor)
        textpos = (width/4, height/4)
        screen.blit(text, textpos)
        if points > 0:
            font = pygame.font.Font(None, 36)
            text = font.render("(But earned " + str(points) + " points.)", 1, config.textcolor)
            textpos = (width/4, height/4 + 70)
            screen.blit(text, textpos)
        endtime = pygame.time.get_ticks() 
        log.writeline("Player looses trial after " + str((endtime - starttime)/1000) + " seconds.")
    player.kill()
    pygame.display.update()
    pygame.time.delay(1000)

log.writeline("Player scored " + str(totalpoints) + " points in total")
log.plotpos()
log.closeLogFiles()
