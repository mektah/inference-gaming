'''
Created on 09.06.2012

@author: Moritz v. Looz
moritz@vlooz.de
'''
import wx, os
import helper, config, distribution
from gobjects import Wave, Saucer, Tripod

class LevelEdit(wx.Frame):
    
    def __init__(self, parent, title):
        self.waveform = None
        wx.Frame.__init__(self, parent, wx.ID_ANY, title=title, size=(700,700))
        self.CreateStatusBar()
        filemenu = wx.Menu()
        
        menuNew = filemenu.Append(wx.ID_NEW, "&New", "Create a new level file")
        menuOpen = filemenu.Append(wx.ID_OPEN, "&Open", "Open a level file")
        menuSave = filemenu.Append(wx.ID_SAVE, "&Save", "Save a level file")
        filemenu.AppendSeparator()
        menuAbout = filemenu.Append(wx.ID_ABOUT, "&About","A simple level editor")
        menuExit = filemenu.Append(wx.ID_EXIT, "E&xit"," Terminate the editor")
        
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")
        self.SetMenuBar(menuBar)
        
        self.Bind(wx.EVT_MENU, self.onAbout, menuAbout)
        self.Bind(wx.EVT_MENU, self.onExit, menuExit)
        self.Bind(wx.EVT_MENU, self.onOpen, menuOpen)
        self.Bind(wx.EVT_MENU, self.onSave, menuSave)
        self.Bind(wx.EVT_MENU, self.onNew, menuNew)
        
        self.scroll = wx.ScrolledWindow(self, -1)
        self.levelsloaded = 0
        self.Show(True)
        
    def onAbout(self,e):
        dlg = wx.MessageDialog(self, "A small level editor", "About this editor", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
        
    def onExit(self,e):
        self.Close(True)
        
    def onOpen(self,e):
        """Open a Levelfile"""
        self.dirname = ''
        dlg = wx.FileDialog(self, "Open a Levelfile", self.dirname, "", "*.levels", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:            
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            self.path = os.path.join(os.path.join(self.dirname, self.filename))
            levels = helper.loadWaves(self.path) # exception needed
            self.buildForm(levels)
        dlg.Destroy()
        
    def onSave(self, e):
        levels = self.waveform.getLevels()
        self.dirname = ''
        dlg = wx.FileDialog(self, "Save as", self.dirname, "", "*.levels", wx.SAVE)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            self.path = os.path.join(os.path.join(self.dirname, self.filename))
            helper.saveWaves(levels, self.path)
        dlg.Destroy()
    
    def onNew(self, e):
        self.scroll.DestroyChildren()
        self.levelsloaded = 0
        dlg = NewLevelDialog(self, 'Choose levels')
        dlg.ShowModal()
        #self.createLevels(config.trialstotal, 3)
    
    def createLevels(self, numberOfTrials, numberOfWaves):
        levels = []
        dist = distribution.Distribution(None)
        for i in range(numberOfTrials):
            right = dist.getSpawnSide()
            spawnheight = dist.getSpawnHeight()
            waves = []
            for j in range(numberOfWaves):
                twave = Wave(spawnheight + j*50, right, Tripod, 5, config.tripodreward, config.tripodfireprobability, config.tripodspeed)
                waves.append(twave)
            levels.append(waves)
        self.buildForm(levels)
        
    def buildForm(self, levels):
        self.scroll.DestroyChildren()
        self.scroll.Scroll(0,0)
        self.outersizer = wx.BoxSizer(wx.VERTICAL)
        self.waveform = WaveForm(self.scroll, levels)
        for sizer in self.waveform.sizerlist:
            self.outersizer.Add(sizer)
        self.scroll.SetSizer(self.outersizer)
        self.scroll.SetAutoLayout(1)
        #self.outersizer.Fit(self.scroll)            
        self.scroll.SetScrollbars(0, self.waveform.dy, 0, self.waveform.y/self.waveform.dy+1)
        self.scroll.SetScrollRate(10, 10)
        self.levelsloaded = 1
            
class NewLevelDialog(wx.Dialog):
    def __init__(self, parent, title):
        '''
        Ask for number of trials, number of waves in each trial
        '''
        super(NewLevelDialog, self).__init__(parent=parent, title=title, size=(250,200))
        self.parent = parent
        panel = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)
        trialbox = wx.StaticBox(panel, label='Trials')
        vbox.Add(trialbox)
        trialsizer = wx.StaticBoxSizer(trialbox, orient=wx.VERTICAL)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(wx.StaticText(panel, -1, label='Number of trials'))
        self.trialbox = wx.TextCtrl(panel, -1, value='35')
        hbox.Add(self.trialbox)
        trialsizer.Add(hbox)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(wx.StaticText(panel, -1, label='Waves in trial'))
        self.wavebox = wx.TextCtrl(panel, -1, value='2')
        hbox.Add(self.wavebox)
        trialsizer.Add(hbox)
        panel.SetSizer(trialsizer)
        
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(panel, proportion=1, flag=wx.ALL|wx.EXPAND, border=5)
        vbox.Add(hbox2, flag= wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=10)

        self.SetSizer(vbox)
        
        okButton.Bind(wx.EVT_BUTTON, self.onOkay)
        closeButton.Bind(wx.EVT_BUTTON, self.onClose)
        
    def onClose(self, e):
        self.Destroy()
        
    def onOkay(self, e):
        numberOfTrials = int(self.trialbox.GetValue())
        numberOfWaves = int(self.wavebox.GetValue())
        self.parent.createLevels(numberOfTrials, numberOfWaves)
        self.Destroy()
        
    
class WaveForm():
    '''list of dictionaries for wave building
    '''
    def __init__(self, parent, levels):
        self.sizerlist = []
        self.formlevels = []
        self.y = 0
        self.dy = 0
        for num, level in enumerate(levels):      
            levelform = []
            panel = wx.Panel(parent)
            levelsizer = wx.BoxSizer(wx.VERTICAL)
            levelsizer.Add(wx.StaticText(panel, -1, 'Trial ' + str(num)))
            self.y += 30
            #height, side, enemytype, number, reward, fireprobability, speed:
            for i, wave in enumerate(level):
                wavedict = {}
                wavesizer = wx.BoxSizer(wx.HORIZONTAL)
                heightbox = wx.TextCtrl(panel, -1, value=str(wave.height) + " ")
                wavesizer.Add(heightbox)
                wavedict['height'] = heightbox
                sidebox = wx.ComboBox(panel, -1, size=(80,-1), choices=['left', 'right'], style=wx.CB_READONLY)
                sidebox.SetSelection(wave.side)
                wavesizer.Add(sidebox)
                wavedict['side'] = sidebox
                numberbox = wx.TextCtrl(panel, -1, value=str(wave.number) + " ")
                wavesizer.Add(numberbox)
                wavedict['number'] = numberbox                
                tpos = config.enemytypes.index(wave.enemytype.__name__)
                typechoice = wx.ComboBox(panel, -1, size=(110,-1), choices=config.enemytypes, style=wx.CB_READONLY)
                typechoice.SetSelection(tpos)
                wavesizer.Add(typechoice)
                wavedict['enemytype'] = typechoice
                rewardbox = wx.TextCtrl(panel, -1, value=str(wave.reward) + " ")
                wavesizer.Add(rewardbox)
                wavedict['reward'] = rewardbox
                fireprobabilitybox = wx.TextCtrl(panel, -1, value=str(wave.fireprobability) + " ")
                wavesizer.Add(fireprobabilitybox)
                wavedict['fireprobability'] = fireprobabilitybox
                speedbox = wx.TextCtrl(panel, -1, value=str(wave.speed) + " ")
                wavesizer.Add(speedbox)
                wavedict['speed'] = speedbox
                wavedict['alive'] = 1
                button = wx.Button(panel, label='Remove wave')
                wavesizer.Add(button)                
                button.Bind(wx.EVT_BUTTON, self.getRemoveFunction(wavedict, wavesizer, num, i), button)
                levelsizer.Add(wavesizer)
                w, h = heightbox.GetSize()
                self.dy = h + 12
                self.y += self.dy
                levelform.append(wavedict)
            self.formlevels.append(levelform)
            panel.SetSizer(levelsizer)
            self.sizerlist.append(panel)
            
    def removeWave(self, wdict, sizer, num, i):
        wdict['alive'] = 0
        print("Removed wave" + str(i) + " of trial" + str(num))
        sizer.Clear(True)
        
        
    def getRemoveFunction(self, wdict, sizer, num, i):
        def remove(e):
            self.removeWave(wdict, sizer, num, i)
        return remove
            
    def getSizers(self):
        return self.sizerlist
        
    def getLevels(self):
        levels = []
        for levelform in self.formlevels:
            level = []
            for wavedict in levelform:
                if not wavedict['alive']:
                    print("Skipped inactive wave")
                    continue
                height = int(wavedict['height'].GetValue())
                side = int(wavedict['side'].GetSelection())
                typeselection = wavedict['enemytype'].GetValue()
                '''The following construct for sure is inelegant. One could replace it by making a list of classes directly. '''
                if typeselection == 'Tripod':
                    enemytype = Tripod
                elif typeselection == 'Saucer':
                    enemytype = Saucer
                number = int(wavedict['number'].GetValue())
                reward = int(wavedict['reward'].GetValue())
                speed = int(wavedict['speed'].GetValue())
                fireprob = float(wavedict['fireprobability'].GetValue())
                wave = Wave(height, side, enemytype, number, reward, fireprob, speed)
                level.append(wave)
            levels.append(level)
        return levels

if __name__ == '__main__':
    app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
    frame = LevelEdit(None, "Trial wave editor") # A Frame is a top-level window.
    app.MainLoop()