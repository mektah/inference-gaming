size = width, height = 800, 600
boltspeed = 20
playerspeed = 10
textspeed = 5
enemydelay = 2000
endtimedelay = 1000
mouseenabled = 0
grabmouse = 0

playersize = (40,40)
boltsize = (3,20)

dirname = '.inference-gaming'
datapath = 'data'
logpath = 'log'
backgroundfile = 'background-4.jpg'
boltfile = "bolt.png"
gunfile = "cannon.png"
shotgunfile = "dish-gun.png"
textcolor =  (200, 200, 100)
warncolor = (200, 0, 0)

minReward = 1
baseReward = 20
timedependantReward = 0
gaussianReward = 0
numberOfEnemies = 10

penalty = 20

gaussianheight = 1
initThreshold = 0.9
trialsBetweenProbabilityShift = 15
trialstotal = 35

'''
Weapon configurations:
'''
gunfirecooldown = 100
gunrewardmultiplier = 2

shotgunfirecooldown = 500
shotgunprojectiles = 10 #
shotgunspread = 45 # Angle in degrees, must not be smaller than number of projectiles
shotgunrewardmultiplier = 1


'''
Enemy configurations:
'''
enemytypes = ['Tripod', 'Saucer']
tripodfile = "enemy-spaceship.png"
tripodsize = (40,40)
tripodspeed = 3
tripodfireprobability = 0.001
tripodscreentime = 5
tripodreward = 10
tripodaim = 1

saucerfile = "saucer.png"
saucersize = (40,10)
saucerspeed = 6
saucerfireprobability = 0.01
saucerscreentime = 3
saucerreward = 50
sauceraim = 1
