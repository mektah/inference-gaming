'''
Created on 25.05.2012

@author: moritz
'''

import pygame, pygame.key, pygame.joystick
import logger, config

class InputDevice():
    
    def hasquit(self):
        return 1
    
class Keyboard(InputDevice):
    def __init__(self):
        self.leftdown = 0
        self.rightdown = 0
        self.fired = 0
        self.quit = 0
        self.mousespeed = 0
    
    def gatherevents(self):
        self.fired = 0
        self.mousemoved = 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit = 1
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_F7:
                    self.quit = 1
                if event.key == pygame.K_LEFT:
                    self.leftdown = 1
                    #log.playermov(trialcounter, currenttime, player.pos[0], "leftstart")
                if event.key == pygame.K_RIGHT:
                    self.rightdown = 1
                    #log.playermov(trialcounter, currenttime, player.pos[0], "rightstart")
                if event.key == pygame.K_SPACE:                    
                    self.fired = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    self.leftdown = 0
                    #log.playermov(trialcounter, currenttime, player.pos[0], "leftstop")
                if event.key == pygame.K_RIGHT:
                    self.rightdown = 0
                    #log.playermov(trialcounter, currenttime, player.pos[0], "rightstop")
            if event.type == pygame.MOUSEMOTION:
                self.mousemoved = 1
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    self.fired = 1
    
    def hasfired(self):
        return self.fired
        
    def hasquit(self):
        return self.quit
    
    def getplayerspeed(self):
        return (-self.leftdown*config.playerspeed + self.rightdown*config.playerspeed + self.mousespeed, 0)
    
    def reset(self):
        self.rightdown = 0
        self.leftdown = 0
        self.fired = 0
    
class Joystick(InputDevice):
    #------------------------------------------------------------------------------
    # probably useful to increase maximum movement with joysticks 
    def __init__(self):
        pygame.joystick.init()
        print("Number of Joysticks:")
        print(str(pygame.joystick.get_count()))
        self.joystick = pygame.joystick.Joystick(0)
        self.joystick.init()
        print("Number of Axes:")
        print(str(self.joystick.get_numaxes()))
        self.axissum =  0
        self.leftdown = 0
        self.rightdown = 0
        self.fired = 0
        self.quit = 0
        self.mousespeed = 0
        self.driftmagicnumber = 0.0#this was only necessary with my old joystick. 
        print("Estimated drift as " + str(self.driftmagicnumber))
    
    def gatherevents(self):
        self.fired = 0
        self.mousemoved = 0
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit = 1
            if event.type == pygame.JOYAXISMOTION:
                if event.axis == 0:
                    self.axissum += event.value
            if event.type == pygame.JOYBUTTONDOWN:
                if event.button == 0:
                    self.fired = 1
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_F7:
                    self.quit = 1
                if event.key == pygame.K_LEFT:
                    self.leftdown = 1
                    #log.playermov(trialcounter, currenttime, player.pos[0], "leftstart")
                if event.key == pygame.K_RIGHT:
                    self.rightdown = 1
                    #log.playermov(trialcounter, currenttime, player.pos[0], "rightstart")
                if event.key == pygame.K_SPACE:                    
                    self.fired = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    self.leftdown = 0
                    #log.playermov(trialcounter, currenttime, player.pos[0], "leftstop")
                if event.key == pygame.K_RIGHT:
                    self.rightdown = 0
                    #log.playermov(trialcounter, currenttime, player.pos[0], "rightstop")
            if event.type == pygame.MOUSEMOTION:
                self.mousemoved = 1
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    self.fired = 1
    
    def hasfired(self):
        return self.fired
    
    def getplayerspeed(self):        
        print("Axissum" + str(self.axissum))
        print("Axpos" + str(self.joystick.get_axis(0)))
        speed = (self.joystick.get_axis(0) + self.driftmagicnumber)*config.playerspeed*2 -self.leftdown*config.playerspeed + self.rightdown*config.playerspeed 
        return (speed, 0)
    
    def reset(self):
        self.rightdown = 0
        self.leftdown = 0
        self.fired = 0
    
    def hasquit(self):
        return self.quit